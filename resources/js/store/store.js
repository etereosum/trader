
import stocks from './modules/stocks.js';
import portfolio from './modules/portfolio.js';
import * as actions from './actions.js';

export default {
    actions,
    modules: {
        stocks,
        portfolio
    }
};