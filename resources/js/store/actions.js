import Axios from "axios"

export const loadData = ({ commit }) => {
    axios.get('api/data')
        .then(res => {
            if (res.data) {
                const stocks = JSON.parse(res.data.stocks);
                const funds = res.data.funds;
                const stockPortfolio = JSON.parse(res.data.stockPortfolio);

                const portfolio = {
                    stockPortfolio,
                    funds
                }

                commit('SET_STOCKS', stocks);
                commit('SET_PORTFOLIO', portfolio);
            }
        });
}