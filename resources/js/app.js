
require('./bootstrap');

window.Vue = require('vue');

// Vue Router

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import { routes } from './routes';

const router = new VueRouter({
    routes,
    mode: 'history'
});

// Vuex

import Vuex from 'vuex';
Vue.use(Vuex);

import storeData from './store/store.js';

const store = new Vuex.Store(storeData);

Vue.component('App', require('./App.vue').default);

// Filters

Vue.filter('currency', (value) => {
    return '$' + value.toLocaleString();
});

// Axios
import axios from 'axios';

// app

const app = new Vue({
    el: '#app',
    router,
    store
});
