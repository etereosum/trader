<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data;
use DB;

class DataController extends Controller
{
    public function store(Request $request)
    {
        DB::table('data')->delete();

        $data = new Data();
        $data->funds = $request->funds;
        $data->stocks = json_encode($request->stocks);
        $data->stockPortfolio = json_encode($request->stockPortfolio);
        $data->save();

        return response()->json(true);
    }

    public function index() 
    {
        $data = Data::first();

        return response()->json($data);
    }
}
