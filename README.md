# Trader

## Requerimientos

Composer

PHP 7.2

Node JS 12.\*

Laravel 5.8

Mysql 8.\*

Git 2.\*

Este es un pequeño ejercicio de integración de Vuejs con el framework Laravel. El proyecto utiliza: vue-router, vuex y axios.

El aplicativo consiste en un panel administrativo donde se muestra primeramente un dashboard con los fondos que posee el usuario (Your Funds: \$1,000).

El menú **Stock** permite ver el valor de las acciones de los diferentes oferentes y también permite comprar acciones (Buy), siempre y cuando estas no excedan el valor de los fondos existentes. Al comprar acciones los fondos (Funds) disminuyen.

En el menú **Portfolio** se observa la cantidad de acciones que ha comprado el usuario (Quantity), con la opción de venderlas (Sell).

La opción **End Day** permite incrementar en un día y generar una variación en los precios (PRICE) de las acciones del Stock.

Finalmente, el usuario puede guardar en memoria el estado de los stocks y el portfolio **Save &amp; Load / SaveData** , permitiendo ser recuperado luego con el munú **Save &amp; Load / LoadData**

## Instalación

\$ git clone [https://gitlab.com/etereosum/trader.git](https://gitlab.com/etereosum/trader.git)

\$ cd trader

Cree un archivo de configuración para el environment

\$ cp .env-example .env

Actualise las variables de entorno e instale las dependencias con composer

composer update

Cree una base de datos y ejecute las migraciones

\$ php artisan migrate

Genere una llave

\$ php artisan key:generate

Actualice las dependencias

\$ php artisa config:cache

\$ npm install

Corra el servidor

\$ npm run dev

\$ php artisan serve
